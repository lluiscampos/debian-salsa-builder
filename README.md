# Lluís Debian packages builder

This is Lluís way of building/testing Debian packages before submitting upstream.
Keep in mind that Debian has its own QA pipelines, this is just an early sanity
check.

## Set up

It assumes the user has in this same repository checked out the Debian repos.

## Build the builder

```
docker build -t debian-salsa-builder docker/
```

## Build a package

To build for example mender-client:

```
docker run --ulimit memlock=131072:131072 --rm -v$(pwd):/debian-salsa --entrypoint bash debian-salsa-builder -c "cd /debian-salsa/mender-client && gbp buildpackage -us -uc"
```

Adjust the command accordingly.

## Note: mender-client depends on mender-artifact

Note that the current builder image installs `mender-artifact` from unstable,
which means that `mender-client` is going to be build using the last published
`mender-artifact`. When `mender-client` new build depends on a `mender-artifact`
update, the build will fail.

To solve this you need build `mender-artifact`, install it, and then build
`mender-client`. This can be done in an interactive session with the builder
with something like:

```
docker run --ulimit memlock=131072:131072 --rm -it -v/home/lluis/northern.work/debian-salsa:/debian-salsa debian-salsa-builder
cd /debian-salsa/golang-github-mendersoftware-mender-artifact
gbp buildpackage -us -uc
dpkg -i /debian-salsa/golang-github-mendersoftware-mender-artifact-dev_*_all.deb
cd /debian-salsa/mender-client
gbp buildpackage -us -uc
```

Also, remember to then bump the `Build-Depends` in mender-client's `debian/control` file.
